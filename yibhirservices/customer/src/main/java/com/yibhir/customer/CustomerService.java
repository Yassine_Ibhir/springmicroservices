package com.yibhir.customer;


import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class CustomerService {

   @Autowired
    private final CustomerRepository customerRepository;

   private final RestTemplate restTemplate;
    public Customer createCustomer(Customer customer) {

        customerRepository.saveAndFlush(customer);
        // todo : check if fraudster
        FraudCheckResponse fraudCheckResponse =
                restTemplate.getForObject(
                "http://localhost:8081/api/v1/fraud-check/{customerId}",FraudCheckResponse.class,
                customer.getId()
        );

        if(fraudCheckResponse.isFraudster()){
            throw new IllegalStateException("fraudster");
        }

        return customer;
        // todo : send notification
    }

    public Iterable<Customer> findAll() {
        return customerRepository.findAll();
    }
}



