package com.yibhir.customer;

public record FraudCheckResponse(boolean isFraudster) {

}
