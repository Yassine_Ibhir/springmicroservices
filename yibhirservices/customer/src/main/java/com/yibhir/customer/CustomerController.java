package com.yibhir.customer;

import com.yibhir.customer.Customer;
import com.yibhir.customer.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/customers")
@Slf4j
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @ResponseBody
    @PostMapping
    public Customer createCustomer(@RequestBody Customer customer) {
        log.info("Create customer {} ",customer);
        return customerService.createCustomer(customer);
    }
    @GetMapping
    public Iterable<Customer> findAllCustomers() {
        return customerService.findAll();
    }
}
