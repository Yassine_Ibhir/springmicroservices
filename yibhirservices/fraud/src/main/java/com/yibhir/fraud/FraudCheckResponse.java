package com.yibhir.fraud;

public record FraudCheckResponse(boolean isFraudster) {

}
